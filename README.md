# K8s Configuration Helm3

helm create app
sudo yum install -y tree

# Helm userful commands
helm ls -n dev
helm install app --wait . -n dev
helm install app app -f ./app/values-dev.yaml -n dev
helm uninstall app -n dev

# Generate Templates before applying
helm template app
helm template app ./app
helm template app ./app -f ./app/values-dev.yaml

# Generate and immediately apply
helm template app
helm template app ./app | kubectl apply -f -
helm template app ./app -f ./app/values.dev.yaml | kubectl apply -f -
